

// Jika user belum login maka HARUS LOGIN terlebih dahulu jika ingin mengakses HALAMAN GAME
if(localStorage.getItem("usernameUser") === null || localStorage.getItem("usernameUser") === "Has Not Been Login!"){
	window.open("http://localhost/tegam_tugas1ppw/Login/login.html","_self");
}else{
	$(document).ready(function(){

		var imgsDir = "imgs/";
		var dafaultImgs = "default_image";
		var imgs = ["0","1","2","3","4","5","6","7","0","1","2","3","4","5","6","7"];
		var extension = ".png";
		var countImgsOpen = 0;
		var prefImg = "init img";
		var prefImgIdOne = "init id";
		var prefImgIdTwo = "init id";
		var solvedImgId = [];
		var lengthSolvedImgs = 0;
		var isStart = false;
		var second = 0;
		var minute = 0;
		var hour = 0;
		var timeLoad;
		var ls = localStorage;
		var bestUser = [];

		makeRandomArray();
		loadImgs();
		loadName();
		loadBestUser();
		orderUserByTime();
		displayBestUser();

		$("#logoutButton").click(function(){
			ls.setItem("usernameUser","Has Not Been Login!");
			window.open("http://localhost/tegam_tugas1ppw/Login/login.html","_self");
		});

		$("#startButton").click(function(){
			prepareToStart();
			isStart = true;
			loadTime();
		});

		$(".imageContainer").click(function(){
			var currentId = $(this).attr("id");
			//jika tombol start sudah diklik dan img yang ada belum terselesaikan, maka akan membuka gambarnya
			if(isStart && prefImgIdOne === currentId){
				//untuk menutup img yg terbuka
				$(this).find("img").animate({
					height: '120px',
		            width: '0px'
				},200,function(){
					countImgsOpen = 0;
					prefImg = "init img";	
					prefImgIdOne = "init id";			
				});
			}else if(isStart && solvedImgId.indexOf(currentId) === -1){
				//membuka gambarnya
				$(this).find("img").animate({
					height: '120px',
		            width: '120px'
				},200, function(){
					//mengecek apakah mengklik img baru atau img yg sudah dibuka
					if(currentId !== prefImgIdOne){
						countImgsOpen += 1;
						if(countImgsOpen == 2){ // membuka 2 img yg ingin dibandingkan
							prefImgIdTwo = currentId;
							if(prefImg === imgs[currentId]){
								imgsMatch();
							}else{
								imgsDontMatch();
							}
							console.log("BUKA KE DUA KALI: "+prefImg+", "+imgs[currentId]);
							countImgsOpen = 0;
							prefImg = "init img";
						}else{ // awal2 membuka img yg belum ke solve
							prefImgIdOne = currentId;
							prefImg = imgs[currentId];
						}
					}
				});
			}else if(!isStart){
				alert("Anda harus menekan tombol START untuk memulai permainan TEGAM :D");
			}

		});

		function loadName(){
			$("#username").html(ls.getItem("usernameUser"));
		}

		function loadTime(){
			timeLoad = setInterval(function(){
				second++;
				if(second === 60){
					minute++;
					second = 0;
				}
				if(minute === 60){
					hour++;
					minute = 0;
				}
				var hourDisplay = hour;
				var minuteDisplay = minute;
				var secondDisplay = second;

				if(hour < 10){
					hourDisplay = "0"+hourDisplay;
				}
				if(minute < 10){
					minuteDisplay = "0"+minuteDisplay;
				}
				if(second < 10){
					secondDisplay = "0"+secondDisplay;
				}
				$("#time").html(hourDisplay+":"+minuteDisplay+":"+secondDisplay);
			},1000);
		}

		function prepareToStart(){

			//membuat img yang random
			makeRandomArray();

			//variable yang berisi id2 img yg telah dibuka
			var openImg = solvedImgId;
			var lengthOpenImgs = lengthSolvedImgs;
			if(prefImgIdOne !== "init id" && solvedImgId.indexOf(prefImgIdOne) === -1){
				openImg.push(prefImgIdOne);
				lengthOpenImgs += 1;
			}

			//flag yang menandakan apakah box yg dishuffle sudah siap untuk ditampilkan atau belum
			var isReadyToDisplay = lengthOpenImgs > 0;

			//Menutup semua box yg sudah diselesaikan (sudah ada pasangannya)
			var i;
			for(i = 0 ; i < lengthOpenImgs ; i++){
				var heightVal = '120px';
				var widthVal = '120px';
				if(i % 2 === 0){
					heightVal = '0px';
				}else{
					widthVal = '0px';
				}
				$("#"+openImg[i]).find("img").animate({
					height: heightVal,
		            width: widthVal
				},200);
			}

			// menghapus interval dan mereload timenya jadi 0
			clearInterval(timeLoad);
			second = 0;
			minute = 0;
			hour = 0;

			//menyetel ulang semua variable
			solvedImgId = [];
			lengthSolvedImgs = 0;
			countImgsOpen = 0;
			prefImg = "init img";
			prefImgIdOne = "init id";
			prefImgIdTwo = "init id";

			//menampilkan semua gambar acak yng ingin didisplay
			if(!isReadyToDisplay){
				//jika tidak ada box yang ditutup maka langsung meload img yg acak
				loadImgs();
			}else{
				//jika ada box yang ditutup maka harus ditutup dulu semua boxnya 
				var interfalToLoadData = setInterval(function(){
					if(i === lengthOpenImgs){
						loadImgs();
						clearInterval(interfalToLoadData);
					}
				},100);
			}

		}

		function imgsMatch(){
			solvedImgId.push(prefImgIdOne);
			solvedImgId.push(prefImgIdTwo);
			lengthSolvedImgs += 2;
			prefImgIdOne = "init id";
			prefImgIdTwo = "init id";
			if(lengthSolvedImgs === 16){
				//jika game telah selesai
				gameFinish();
			}
		}

		function imgsDontMatch(){
			
			$("#"+prefImgIdOne).find("img").animate({
				height: '0px',
	            width: '120px'
			},200,function(){
				prefImgIdOne = "init id";
			});
			$("#"+prefImgIdTwo).find("img").animate({
				height: '120px',
	            width: '0px'
			},200,function(){
				prefImgIdTwo = "init id";
			});
		}

		function gameFinish(){
			console.log("game finish");
			clearInterval(timeLoad);
			var totalWaktu = hour*60*60+minute*60+second;
			var username = $("#username").html();
			var user = {name:username, time:totalWaktu};
			var indexOfUser = 0;
			for(i = 0 ; i < bestUser.length ; i++){
				if(username === bestUser[i].name){
					if(totalWaktu >= bestUser[i].time){
						return 0;
					}
					bestUser[i].time = totalWaktu;
					orderUserByTime();
					displayBestUser();
					sendDataToLocalStorage();
					return 0;
				}else if(totalWaktu > bestUser[i].time){
					indexOfUser = i;
				}
			}
			bestUser.splice(indexOfUser,0,user);
			sendDataToLocalStorage();
			loadBestUser();
			orderUserByTime();
			displayBestUser();
		}

		function sendDataToLocalStorage(){
			var currentUserName = ls.getItem("usernameUser");
			ls.clear();
			ls.setItem("usernameUser",currentUserName);
			for(i = 0 ; i < 5 && i < bestUser.length ; i++){
				ls.setItem(bestUser[i].name,bestUser[i].time);
			}
		}


		function getImg(img){
			return imgsDir+img+extension;
		}


		function makeRandomArray(){
			var randomIndex;
			var temptArray = [];
			while(imgs.length > 0){
				randomIndex = Math.floor(Math.random() * imgs.length);
				temptArray.push(imgs[randomIndex]);
				imgs.splice(randomIndex, 1);
			}
			imgs = temptArray;
		}

		function loadImgs(){
			var countLog = [];
			for(i = 0 ; i < imgs.length ; i++){
				var currentElementID = "#"+i;
				$(currentElementID+" img").attr("src",getImg(imgs[i]));

				if(countLog.indexOf(getImg(imgs[i])) !== -1){
					countLog.splice( countLog.indexOf(getImg(imgs[i])) , 1);
				}else{
					countLog.push(getImg(imgs[i]));
				}

			}
		}

		function loadBestUser(){
			bestUser = [];
			for(i = 0 ; i < ls.length ; i++){
				var currentName = ls.key(i);
				if(currentName !== "usernameUser"){
					var currentTime = Number(ls.getItem(currentName));
					var currentUser = {name:currentName, time:currentTime};
					bestUser.push(currentUser);
				}
			}
		}

		function displayBestUser(){
			console.log("display best user");
			$("#listOfBestScore").empty();
			for(i = 0 ; i < bestUser.length ; i++){
				var currentUser = bestUser[i];
				$("#listOfBestScore").append("<li>"+currentUser.name+" , "+currentUser.time+" second"+"</li>");
			}
		}

		function orderUserByTime(){
			var output = [];
			while(bestUser.length > 0){
				var indexOfStrongest = 0;
				for(i = 1 ; i < bestUser.length ; i++){
					if(bestUser[indexOfStrongest].time > bestUser[i].time){
						indexOfStrongest = i;
					}
				}
				output.push(bestUser[indexOfStrongest]);
				bestUser.splice(indexOfStrongest, 1);
			}
			bestUser = output;
		}

	});
}

